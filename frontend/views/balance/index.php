<?php
use frontend\assets\GraphicsAsset;
use yii\widgets\LinkPager;
use yii\bootstrap\ButtonDropdown;

/* @var $this yii\web\View */
/* @var $history common\models\rebate\History */

$this->title = 'История операций';
?>
<style>
    * {
        box-sizing: border-box;
    }
</style>
<div class="content__bottom">
    <div class="history">
        <span class="title-block">история операций</span>
        <p>Ваш текущий баланс: <strong><?= Yii::$app->user->identity->balance ?> руб.</strong></p>
        <br>
        <div class="history__table">
            <table class="rtable rtable--flip">
                <thead>
                <tr>
                    <th>Дата</th>
                    <th>Сумма</th>
                    <th>До операции</th>
                    <th>После операции</th>
                    <th>Примечание</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($history) {
                    foreach ($history as $item) {
                        ?>
                        <tr>
                            <td><?= Yii::$app->formatter->asDate($item->operation_date) ?></td>
                            <td><?= $item->credit ?></td>
                            <td><?= $item->before ?></td>
                            <td><?= $item->after ?></td>
                            <td><?= $item->note ?></td>
                        </tr>
                        <?php
                    }

                } else {
                    ?>
                    <tr>
                        <td colspan="5">Операций нет</td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <?php
            // отображаем постраничную разбивку
            echo LinkPager::widget([
                'pagination' => $pages,
                'firstPageLabel' => 'Первая',
                'lastPageLabel' => 'Последняя'
            ]);
            $pageSizeLimit = $pages->getpageSize();
            $label = $pageSizeLimit == 100000 ? 'Все' : $pageSizeLimit;
            echo ButtonDropdown::widget([
                'containerOptions' => ['class' => 'button_drop_down'],
                'label' => $label,
                'options' => [
                    'class' => 'btn-lg btn-default',
                ],
                'dropdown' => [
                    'items' => [
                        [
                            'label' => '20',
                            'url' => '?limit=20'
                        ],
                        [
                            'label' => '50',
                            'url' => '?limit=50'
                        ],
                        [
                            'label' => '100',
                            'url' => '?limit=100'
                        ],
                        [
                            'label' => '200',
                            'url' => '?limit=200'
                        ],
                        [
                            'label' => '500',
                            'url' => '?limit=500'
                        ],
                        [
                            'label' => 'Все',
                            'url' => '?limit=all'
                        ],
                    ]
                ]
            ]);
            ?>
            <div class="button_drop_down btn-group">
                <span>записей</span>
            </div>

        </div>
    </div>
</div>