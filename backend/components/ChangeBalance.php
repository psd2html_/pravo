<?php
namespace backend\components;

use Yii;
use yii\base\Behavior;
use common\models\User;
use common\models\History;
use ErrorException;

class ChangeBalance extends Behavior
{
    /**
     * Change balance
     * @param User $user
     * @param $balance string
     * @param $note string
     * @param $deducted bool
     *
     * @return bool
     */
    public function changeBalance(User $user, $balance, $note='', $deducted = false)
    {
        $transaction = User::getDb()->beginTransaction();
        try {
            $param['before'] = $user->balance;
            $user->scenario = User::SCENARIO_BALANCE;
            if ($deducted) {
                $user->balance -= $balance;
            } else {
                $user->balance += $balance;
            }
            $param['after'] = $user->balance;
            if($user->balance < 0) {
                throw new ErrorException('Отрицательный баланс');
            }
            $user->save();

            //Сохраняем в журнал
            $param['id_user'] = $user->id;
            $param['credit'] = $balance;
            $param['note'] = $note;

            if(!$this->journalSave($param)) {
                throw new ErrorException('Не удалось сделать запись в таблицу "history"');
            }
            $note_zzz = 'Пользователь ' . $user->fullName() .
                ' оплатил тариф ' . $balance;
            if($deducted && $user->ref_id) {

                $ref_balance = $balance / 100 * 52;
                $worker = User::findOne($user->ref_id);
                if($worker) {
                    $param['before'] = $worker->balance;
                    $worker->balance += $ref_balance;
                    $param['after'] = $worker->balance;
                    $worker->save();
                    //Сохраняем в журнал
                    $param['id_user'] = $user->ref_id;
                    $param['credit'] = $ref_balance;
                    $param['note'] = $note_zzz;
                    if(!$this->journalSave($param)) {
                        throw new ErrorException('Не удалось сделать запись в таблицу "history"');
                    }
                }
            }
            $owner_balance = $balance / 100 * 16;
            if(!$this->changeOwnersBalance($owner_balance, $note_zzz)) {

                throw new ErrorException('Ошибка пополнения баланса');
            }
            $transaction->commit();

            return true;
        } catch(ErrorException $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', $e->getMessage());

            return false;
        }
    }

    /**
     * Change balance
     * @param User $user
     * @param $balance string
     * @param $note string
     * @param $company string
     * @param $deducted bool
     *
     * @return bool
     */
    public function changeOwnersBalance($balance, $note)
    {
        try {
            $owners_ids = User::find()->select(['id', 'balance'])->where(['is_worker' => 2])->limit(3)->all();

            if($owners_ids) {
                foreach ($owners_ids as $owner) {
                    $param['before'] = $owner->balance;
                    $owner->balance += $balance;
                    $param['after'] = $owner->balance;
                    if($owner->balance < 0) {
                        throw new ErrorException('Отрицательный баланс');
                    }
                    $owner->save();

                    //Сохраняем в журнал
                    $param['id_user'] = $owner->id;
                    $param['credit'] = $balance;
                    $param['note'] = $note;

                    if(!$this->journalSave($param)) {
                        throw new ErrorException('Не удалось сделать запись в таблицу "history"');
                    }
                }
            }

            return true;
        } catch(ErrorException $e) {

            return false;
        }
    }

    private function journalSave($param)
    {
        $history = new History();
        $history->id_user = $param['id_user'];
        $history->operation_date = date('Y-m-d H:i:s');
        $history->credit = $param['credit'];
        $history->note = $param['note'];
        $history->before = $param['before'];
        $history->after = $param['after'];


        if(!$history->save()) {

            return false;
        }

        return true;
    }
}