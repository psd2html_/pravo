<?php

namespace backend\controllers;

use Yii;
use backend\models\SystemLog;
use backend\models\SystemLogSeach;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SystemLogController implements the CRUD actions for SystemLog model.
 */
class SystemLogController extends SiteController
{
    /**
     * Lists all SystemLog models.
     * @return mixed
     */
    public function actionIndex()
    {
        if($post = Yii::$app->request->post()) {
            if(isset($post['action'])) {
                switch ($post['action']) {
                    case 'del_selected':
                        if(isset($post['del_errors'])) {
                            SystemLog::deleteAll(['IN', 'id', $post['del_errors']]);
                            Yii::$app->session->setFlash('success', 'Выбранные логи успешно удалены');
                        }
                        break;
                    case 'del_all':
                        SystemLog::deleteAll();
                        Yii::$app->session->setFlash('success', 'Все логи успешно удалены');
                        break;
                }
            }

        }
        $searchModel = new SystemLogSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SystemLog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the SystemLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SystemLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SystemLog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
