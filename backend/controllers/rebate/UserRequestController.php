<?php

namespace backend\controllers\rebate;

use Yii;
use common\models\rebate\SearchUserRequest;
use common\models\rebate\UserRequest;
use common\models\User;

class UserRequestController extends \backend\controllers\SiteController
{
    /**
     * Lists all Request models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchUserRequest();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Update status.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateStatus()
    {
        if(!Yii::$app->request->isAjax) {
            exit('go home!');
        }
        $updated_rows = 0;
        if($post = Yii::$app->request->post()) {
            $updated_rows = UserRequest::updateAll(['status' => $post['status']], ['in', 'id', $post['keys']]);
        }
        if($updated_rows > 0) {
            if($post['status'] == 1) {
                $temp = 4;
            } else {
                $temp = 5;
            }
            foreach ($post['keys'] as $userRequestId) {
                $userRequest = UserRequest::findOne($userRequestId);
                $user = $userRequest->idUser;
                $params = [
                    'company' => $userRequest->getCompanyName(),
                    'section' => $userRequest->idSection->title,
                    'account' => $userRequest->account,
                ];
                $user->sendMail($temp, $params);
            }
            echo 'ok';
        } else {
            echo 'error';
        }
    }

}
