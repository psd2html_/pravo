<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "configure".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 */
class Configure extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'configure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'value'], 'required'],
            [['key'], 'string', 'max' => 250],
            [['value'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Параметр',
            'value' => 'Значение',
        ];
    }
}
