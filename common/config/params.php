<?php
return [
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'adminEmail' => 'pinaev@meta.ua',
    'noPhoto' => 'no_photo.jpg',
    'avatarURL' => '/uploads/avatar/'
];
