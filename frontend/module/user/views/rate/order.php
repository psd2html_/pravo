<?php
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Rate */

$this->title = 'Оплата Тарифа';
?>

<div class="content__section">
    <div class="all-bid">
        <span class="title-block"><?= $this->title ?></span>
        <div class="all-bid__table">
            <table class="rtable rtable--flip">
                <thead>
                </thead>
                <tbody>
                <tr>
                    <td>Оплатить тариф</td>

                    <td>Пополнить баланс</td>
                </tr>
                <tr>
                    <td>
                        <?= $model->summ ?>
                        <br>
                        <?= $model->description ?>
                    </td>
                    <td>
                        Ваш баланс: <?= Yii::$app->user->identity->balance ?>руб.
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php $form = ActiveForm::begin(); ?>
                            <button class="btn">Оплатить</button>
                        <?php ActiveForm::end(); ?>
                    </td>
                    <td><a href="#"><button class="btn">Пополнить</button></a></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>