<?php
$service = isset($_SESSION['service']) ? $_SESSION['service'] : null;
?>
<header class="header">
    <div class="block__container">
        <div class="logo">
        </div>
        <div class="authorization">
            <a href="/site/logout" class="logout btn_main">Выйти</a>
        </div>
        <div class="user">
            <div class="user__info user__info_height">
                <div class="user__photo">
                </div>
                <div class="user__description">
                    <span class="user__title"><?= Yii::$app->user->identity->first_name ?> <?= Yii::$app->user->identity->last_name ?></span>
                    <span class="user__subtitle"><?= Yii::$app->user->identity->email ?></span>
                </div>
            </div>
            <div class="user__info user__info_margin">
                <div class="user__ico">
                    <img src="/img/balance.png" alt="balance_photo">
                </div>
                <div class="user__description">
                    <span class="user__title">Баланс</span>
                    <span class="user__subtitle"><?= Yii::$app->user->identity->balance ?>руб.</span>
                </div>
            </div>
        </div>
    </div>
</header>