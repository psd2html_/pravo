<?php
namespace frontend\widgets;

use yii\base\Widget;
use common\models\rebate\StaticBlock;

/*
 * Static block
 */
class StaticBlockWidget  extends Widget
{
    public $id;
    public $html;

    public function init()
    {
        parent::init();

        $this->html = '';
        if (($model = StaticBlock::findOne($this->id))) {
            $this->html = $model->html;
        }
    }

    public function run()
    {
        return $this->html;
    }
}