<?php

namespace frontend\models;

use Yii;
use common\models\User;
use yii\base\InvalidParamException;

/**
 * This is the model class for table "email_confirm".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $email
 * @property string $token
 */
class EmailConfirm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_confirm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Этот адрес электронной почты уже занят.'],
            ['email', 'unique', 'targetClass' => '\frontend\models\EmailConfirm', 'message' => 'Этот адрес электронной почты уже занят.'],
            [['user_id'], 'integer'],
            [['email', 'token'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Новый email',
        ];
    }

    /**
     * Update email
     *
     * @param  string $token
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function updateEmail($token)
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Отсутствует код подтверждения.');
        }
        $item = self::find()->where(['token' => $token])->one();
        if (!$item) {
            throw new InvalidParamException('Неверный токен.');
        }

        $user = User::findOne($item->user_id);
        $user->email = $item->email;

        if($user->save()) {

            self::deleteAll(['user_id' => $user->id]);

            return true;
        }

        return false;
    }
}
