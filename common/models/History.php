<?php

namespace common\models;

use Yii;
use common\models\User;
use backend\models\Pay;
use common\models\Admin;

/**
 * This is the model class for table "history".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $operation_date
 * @property double $credit
 * @property string $note
 * @property double $before
 * @property double $after
 *
 * @property User $idUser
 */
class History extends \yii\db\ActiveRecord
{
    public $sum;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'operation_date', 'credit'], 'required'],
            [['id_user'], 'integer'],
            [['credit', 'before' , 'after'], 'number'],
            [['note'], 'string', 'max' => 255],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fullName' => 'Имя Фамилия',
            'operation_date' => 'Дата операции',
            'credit' => 'Сумма($)',
            'note' => 'Примечание',


        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Admin::className(), ['id' => 'id_admin']);
    }

    /* Геттер для полного имени человека */
    public function getFullName()
    {
        $full_name = '';
        if($this->user) {
            $full_name = $this->user->getFullName();
        }

        return $full_name;

    }

    /* Геттер для баланса*/
    public function getBalance()
    {
        $balance = 0;
        if($this->user) {
            $balance = $this->user->balance;
        }

        return $balance;
    }
}
