<?php
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\rebate\SearchBaseSection */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="content__bottom">
    <div class="history">
        <span class="title-block">Управление сотрудниками</span>
        <div class="history__table">
            <div>
                <?= GridView::widget([
                    'pager' => [
                        'firstPageLabel' => 'Первая страница',
                        'lastPageLabel'  => 'Последняя страница'
                    ],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'summary' => 'Показано {count} из {totalCount}',
                    'columns' => [
                        'id',
                        [
                            'attribute' => 'first_name',
                            'label'=>'Сотрудник',
                            'content' => function($data) {
                                return $data->fullName();
                            }
                        ],
                        'phone',
                        'email',
                        [
                            'attribute'=>'birthdate',
                            'format' => ['date', 'php: d.m.Y'],
                            'filter' => DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'birthdate',
                                'clientOptions' => [
                                    'autoclose' => true,
                                    'format' => 'dd.mm.yyyy'
                                ]
                            ])
                        ],
                        [
                            'label' => 'Ссылка',
                            'format' => 'raw',
                            'value' => function($data){

                                return Html::a(
                                    '<button class="btn">Смотреть</button>',
                                    '/owner/customers/?id='. $data->id,
                                    [
                                        'title' => 'Смотреть',
                                    ]
                                );
                            }
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
