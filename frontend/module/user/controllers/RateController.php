<?php
namespace frontend\module\user\controllers;

use Yii;
use common\models\Rate;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use backend\components\ChangeBalance;
use common\models\User;


class RateController extends Controller
{
    public $layout = 'vpluse';


    /**
     * Show rates.
     * @return mixed
     */
    public function actionIndex()
    {
        $rates = Rate::find()->all();
        $current_rate = Yii::$app->user->identity->rate_id;
        $current_rate_info = 0;
        if($current_rate) {
            $current_rate_info = $this->findModel($current_rate);
        }

        return $this->render('index', [
            'rates' => $rates,
            'current_rate' => $current_rate,
            'current_rate_info' => $current_rate_info,

        ]);
    }

    /**
     * Order rates.
     * @return mixed
     */
    public function actionOrder($id)
    {
        $model = $this->findModel($id);
        if(!$this->checkRate($model)) {
            Yii::$app->session->setFlash('error', 'Данный тариф не доступен');

            return $this->redirect(['index']);
        }
        if(Yii::$app->request->isPost) {
            $balance = $model->summ;
            if(Yii::$app->user->identity->rate_id) {
                $current_rate_info = $this->findModel(Yii::$app->user->identity->rate_id);
                $balance -= $current_rate_info->summ;
            }

            if($balance > Yii::$app->user->identity->balance) {
                Yii::$app->session->setFlash('error', 'На вашем счету недостаточно средств. Пополните баланс.');
            } else {
                $change_balance = new ChangeBalance();
                $user = $this->findModelUser();
                if($change_balance->changeBalance($user, $balance, 'Оплата тарифа ' . $model->summ, true)) {
                    $user->rate_id = $id;
                    $user->save();
                    Yii::$app->session->setFlash('success', 'Вы приобрели тариф ' . $model->summ);
                    return $this->redirect(['index']);
                }
            }
        }

        return $this->render('order', [
            'model' => $model,

        ]);
    }

    /**
     * Finds the Section model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rate::findOne($id)) !== null) {

            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelUser()
    {
        if (($model = User::findOne(Yii::$app->user->identity->id)) !== null) {

            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function checkRate($model)
    {
        if(Yii::$app->user->identity->rate_id) {
            $current_rate_info = $this->findModel(Yii::$app->user->identity->rate_id);
            if($model->summ <= $current_rate_info->summ) {

                return false;
            }
        }
        return true;
    }
}
