<?php
use frontend\assets\VpluseAsset;
use yii\helpers\Html;
use common\widgets\Alert;
use frontend\widgets\ServiceMenuWidget;

VpluseAsset::register($this);
$this->beginPage();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div class="layer"></div>
        <div class="wrapper">
            <!-- BEGIN HEADER -->
            <?= $this->render('//vpluse/head') ?>
            <!-- HEADER EOF -->

            <!-- BEGIN SIDEBAR -->
            <?php
            $service = isset($_SESSION['service']) ? $_SESSION['service'] : null;
            switch ($service) {
                case 'user' :
                    echo $this->render('//vpluse/user_left');
                    break;
                case 'worker' :
                    echo $this->render('//vpluse/worker_left');
                    break;
                case 'owner' :
                    echo $this->render('//vpluse/owner_left');
                    break;
                default : echo $this->render('//vpluse/user_left');
            }
            ?>
            <!-- SIDEBAR EOF -->

            <!-- BEGIN CONTENT -->
            <section class="content">
                <div class="block__container">
                    <div class="content__main">
                        <?= Alert::widget() ?>
                        <?= $content ?>
                    </div>
                </div>
            </section>
            <section class="content__addit content__addit_gray">
                <?= $this->render('//vpluse/right') ?>
            </section>
            <!-- CONTENT EOF -->
        </div>

        <?php $this->endBody() ?>
        <?php
            $configure = Yii::$app->configure->getData();
            echo $configure['footerScripts'];
        ?>
    </body>
</html>
<?php $this->endPage() ?>