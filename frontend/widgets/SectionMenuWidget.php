<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use common\models\rebate\BaseSection;
use common\models\rebate\Section;
use yii\helpers\Url;

/*
 * NewsWidget::begin();
 * <p>:date</p>
 * <h1>:title</h1>
 * NewsWidget::end();
 */
class SectionMenuWidget  extends Widget
{
    public $menu_sections = [];


    public function init()
    {
        parent::init();

        $base_sections = BaseSection::find()->all();
        $sections = Section::find()->all();
        if ($base_sections) {
            foreach ($base_sections as $base_section) {
                if ($sections) {
                    foreach ($sections as $section) {
                        if ($section['id_base'] == $base_section['id']) {
                            $this->menu_sections[$base_section['name']][] = ['title' => $section['title'], 'id' => $section['id']];
                        }

                    }
                }
            }
        }
    }

    public function run()
    {
        if ($this->menu_sections) {
            $current_section_id = 0;
            if(isset(Yii::$app->request->queryParams['id'])) {
                $current_section_id = Yii::$app->request->queryParams['id'];
            }
            $icons = ['item-trader', 'item-investor', 'item-webmaster'];
            $i = 0;
            foreach ($this->menu_sections as $base => $sections ) {

                $active = '';
                $section_ids = [];
                foreach ($sections as $section) {
                    $section_ids[] = $section['id'];
                }
                if(in_array($current_section_id, $section_ids)) {
                    $active = ' active';
                }
                echo '<li class="sidebar__row">';
                echo '<a class="sidebar__item sidebar__' . $icons[$i] . $active . ' js-drop" href="#?">';
                echo '<span class="sidebar__title">' . $base . '</span>';
                echo '</a>';
                if ($sections) {
                    echo '<ul class="dropdown">';
                    foreach ($sections as $section) {
                        $active = '';
                        if($section['id'] == $current_section_id) {
                            $active = ' active';
                        }
                        echo '<li class="dropdown__row">';
                        echo '<a class="dropdown__item' . $active . '" href="' . Url::to(['/rebate/section', 'id' => $section['id']]) . '">' . $section['title'] . '</a>';
                        echo '</li>';
                    }
                    echo '</ul>';
                }
                $i++;
            }
        }
    }
}