<?php

use dosamigos\datepicker\DatePicker;
use backend\widgets\CustomGridWidget;
use common\models\rebate\Company;
use yii\bootstrap\Button;

/* @var $this yii\web\View */
/* @var $searchModel common\models\rebate\SearchCompany */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список счетов';
$this->registerJsFile('js/change_status.js', ['depends' => ['yii\web\JqueryAsset']]);
?>
<div class="box box-warning">
    <div class="box-header">
        <h3 class="box-title"><?= $this->title ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?php
            echo Button::widget([
                'label' => 'Подтвердить',
                'options' => [
                    'class' => 'btn-success change_user_request_status',
                    'style' => 'margin:5px',
                    'status' => '1',
                ]
            ]);
            echo Button::widget([
                'label' => 'Отклонить',
                'options' => [
                    'class' => 'btn-danger change_user_request_status',
                    'style' => 'margin:5px',
                    'status' => '2',
                ]
            ]);
            ?>
            <i id="loader" style="display: none" class="fa fa-refresh fa-spin"></i>
        </p>
        <?= CustomGridWidget::widget([
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'name' => 'requests[]',
                    'checkboxOptions' => function($data, $key) {
                        return ['value' => $key ];
                    },
                ],
                [
                    'attribute'=>'status',
                    'format'=>'text',
                    'content'=>function($data){
                        switch ($data->status) {
                            case 0 : $color = 'primary'; break;
                            case 1: $color = 'success'; break;
                            case 2 : $color = 'danger'; break;
                            default: $color = 'warning'; break;
                        }
                        return '<span class="label label-' . $color . '">' . $data->statusName . '</span>';
                    },
                    'filter'=>$searchModel->getStatusArrayNames()
                ],

                'fullName',
                'login',
                'companyName',
                [
                    'attribute'=>'id_section',
                    'format'=>'text',
                    'content'=>function($data){
                        return $data->getSectionTitle();
                    },
                    'filter'=> Company::getSectionsList()
                ],
                'account',
                [
                    'attribute'=>'created_at',
                    'format' => ['date', 'php: d.m.Y'],
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ])
                ],
            ],
        ]); ?>
    </div>
</div>
