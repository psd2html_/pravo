<?php
use yii\helpers\Url;

$current_action = Yii::$app->controller->action->id;
$current_controller = Yii::$app->controller->id;
$active_balance = '';
if($current_controller == 'pay' || $current_action == 'history') {
    $active_balance = ' active';
}
?>
<div class="sidebar">
    <ul class="sidebar__nav">
        <li class="sidebar__row">
            <a class="sidebar__item sidebar__item-support<?php echo $current_controller == 'worker' && $current_action == 'index' ? ' active-empty' : ''?>" href="/worker">
                <span class="sidebar__title">Приглашение</span>
            </a>
        </li>
        <li class="sidebar__row">
            <a class="sidebar__item sidebar__item-profile<?php echo $current_action == 'customers' ? ' active-empty' : ''?>" href="/worker/customers">
                <span class="sidebar__title">Клиенты</span>
            </a>
        </li>
        <li class="sidebar__row">
            <a class="sidebar__item sidebar__item-balance<?php echo $current_controller == 'balance' ? ' active-empty' : ''?>" href="<?= Url::to(['/balance']) ?>">
                <span class="sidebar__title">Финансы</span>
            </a>
        </li>
    </ul>
    <a class="show-menu" href="#?">
        <span class="show-menu__ico"></span>
        <span class="show-menu__title">свернуть</span>
    </a>
</div>