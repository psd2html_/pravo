<?php

use backend\widgets\CustomGridWidget;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SystemLogSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Системные ошибки';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3 class="box-title"><?= $this->title ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(['id' => 'error_log_form']); ?>
        <div class="form-group">
            <?= Html::dropDownList('action', '', [
                '' => 'Выберете действие',
                'del_selected' => 'Удалить выбранное',
                'del_all' => 'Удалить всё'
            ], [
                'id' => 'error_log_action',
                'class' => 'btn-sm btn-default',
            ]) ?>
            <?= Html::button('Выполнить', [
                'id' => 'error_log_action_button',
                'class' => 'btn btn-primary',
                'disabled' => true]) ?>
        </div>
        <?= CustomGridWidget::widget([
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'name' => 'del_errors[]',
                    'checkboxOptions' => function($data, $key) {
                        return ['value' => $key ];
                    },
                ],
                'level',
                'category',
                [
                    'attribute'=>'log_time',
                    'content'=>function($data){
                        return date('d.m.Y H:i:s', $data->log_time);
                    }
                ],
                'prefix:ntext',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                ],
            ],
        ]); ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
