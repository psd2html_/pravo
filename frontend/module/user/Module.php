<?php

namespace frontend\module\user;

use Yii;

/**
 * user module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\module\user\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        parent::init();

        $this->setLayoutPath('@frontend/views/layouts');
        $session = Yii::$app->session;
        $session->set('service', 'user');
        // custom initialization code goes here
    }
}
