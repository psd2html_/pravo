<?php
use backend\widgets\CustomGridWidget;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\\SearchUser */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Управление пользователями';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3 class="box-title"><?= $this->title ?></h3>
    </div>
    <div class="box-body">
        <?= CustomGridWidget::widget([
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'id',
                'first_name',
                'last_name',
                'email:email',
                [
                    'attribute'=>'created_at',
                    'format' => ['date', 'php: d.m.Y'],
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ])
                ],


                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
