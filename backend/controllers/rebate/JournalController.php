<?php
namespace backend\controllers\rebate;

use Yii;
use common\models\SearchUser;
use common\models\rebate\HistorySearch;
use common\models\User;
use yii\web\NotFoundHttpException;

class JournalController extends \backend\controllers\SiteController
{


    /**
     * Lists all Request models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
