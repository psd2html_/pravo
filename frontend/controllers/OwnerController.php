<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\User;
use common\models\SearchUser;
use yii\web\ForbiddenHttpException;
use common\models\History;

/**
 * Site controller
 */
class OwnerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function init()
    {
        parent::init();
        $this->layout = 'vpluse';
        $session = Yii::$app->session;
        $session->set('service', 'owner');
        if(Yii::$app->user->identity->is_worker != 2) {
            throw new ForbiddenHttpException(Yii::t('yii', 'У вас нету прав для просмотра этого раздела'));
        }
    }

    /**
     * Displays partner account.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchUser();
        $query = User::find()->where(['is_worker' => 1]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $query);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays partner account.
     *
     * @return mixed
     */
    public function actionCustomers($id)
    {
        $worker = User::findOne($id);

        $searchModel = new SearchUser();
        $query = User::find()->where(['ref_id' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $query);

        return $this->render('customers', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'worker' => $worker->fullName(),
        ]);
    }

    /**
     * Displays partner account.
     *
     * @return mixed
     */
    public function actionHistory()
    {
        $history = History::find()
            ->where(['id_user' => Yii::$app->user->id])
            ->andWhere(['in', 'orientation', [2,3]])
            ->orderBy('id DESC')->all();

        return $this->render('history', ['history' => $history]);
    }
}
