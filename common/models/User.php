<?php

namespace common\models;

use Yii;
use frontend\components\Helper;
use yii\base\NotSupportedException;
use yii\base\InvalidParamException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email_confirm_token
 * @property string $email
 * @property string $phone
 * @property string $first_name
 * @property string $last_name
 * @property string $balance
 * @property string $patronymic
 * @property integer $ref_id
 * @property string $ref_code
 * @property integer $status
 * @property integer $created_at
 * @property integer $city_id
 * @property string $birthdate
 * @property string $birthplace
 * @property string $passport_series
 * @property integer $passport_number
 * @property string $passport_issued
 * @property string $passport_issued_date
 * @property integer $department_code
 * @property string $registered_address
 * @property string $residential_address
 * @property integer $is_worker
 * @property integer $rate_id
 *
 * @property History[] $histories
 * @property Output[] $outputs
 * @property UserGroup[] $userGroups
 * @property UserPay[] $userPays
 * @property UserRequest[] $userRequests
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $current_pas;
    public $new_pas;
    public $new_pas_repeat;

    const SCENARIO_PROFILE = 'profile';
    const SCENARIO_BALANCE = 'balance';
    const STATUS_DELETED = 0;
    const STATUS_BLOCKED = 2;
    const STATUS_WAIT = 5;
    const STATUS_ACTIVE = 10;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => function(){ return date('Y-m-d');},
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['balance'], 'number'],
            [['ref_id', 'status', 'created_at', 'city_id', 'passport_number', 'department_code'], 'integer'],
            [['birthdate', 'passport_issued_date'], 'safe'],
            [['passport_issued', 'registered_address', 'residential_address', 'phone'], 'string'],
            [['password_hash', 'password_reset_token', 'email', 'first_name', 'last_name', 'patronymic'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['ref_code'], 'string', 'max' => 8],
            [['birthplace'], 'string', 'max' => 250],
            [['passport_series'], 'string', 'max' => 5],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'phone' => 'Телефон',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'balance' => 'Баланс',
            'patronymic' => 'Отчество',
            'ref_id' => 'Ref ID',
            'ref_code' => 'Ref Code',
            'status' => 'Status',
            'created_at' => 'Дата регистрации',
            'city_id' => 'Город',
            'birthdate' => 'Дата рождения',
            'birthplace' => 'Место рождения',
            'passport_series' => 'Серия паспорта',
            'passport_number' => 'Номер паспорта',
            'passport_issued' => 'Паспорт выдан',
            'passport_issued_date' => 'Дата выдачи паспорта',
            'department_code' => 'Код подразделения',
            'registered_address' => 'Адрес регистрации',
            'residential_address' => 'Адрес проживания',
            'rate_id' => 'Тариф',
        ];
    }

    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_PROFILE => [
                'phone',
                'first_name',
                'last_name',
                'patronymic',
                'city_id',
                'birthdate',
                'birthplace',
                'passport_series',
                'passport_number',
                'passport_issued',
                'passport_issued_date',
                'department_code',
                'registered_address',
                'residential_address'
            ],
            self::SCENARIO_BALANCE => ['balance'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        try {
            return Yii::$app->security->validatePassword($password, $this->password_hash);

        } catch (InvalidParamException $e) {
            $this->addError('Неверный формат');
        }

    }

    /**
     * @param string $email_confirm_token
     * @return static|null
     */
    public static function findByEmailConfirmToken($email_confirm_token)
    {
        return static::findOne(['email_confirm_token' => $email_confirm_token, 'status' => self::STATUS_WAIT]);
    }

    /**
     * Generates email confirmation token
     */
    public function generateEmailConfirmToken()
    {
        $this->email_confirm_token = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates ref code
     */
    public function generateRefCode()
    {
        do {
            $this->ref_code = Helper::random_string();

        } while (User::find()->where(['ref_code' => $this->ref_code])->one());
    }

    /**
     * Set ref id
     */
    public function setReferral()
    {
        $cookies = Yii::$app->request->cookies;
        if (($cookie = $cookies->get('referral')) !== null) {
            $referral = $cookie->value;
            $referral_user = self::find()->select(['id'])->where(['ref_code' => $referral])->one();
            if($referral_user) {
                $this->ref_id = $referral_user->id;
            }
        }
    }

    /**
     * Removes email confirmation token
     */
    public function removeEmailConfirmToken()
    {
        $this->email_confirm_token = null;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return integer
     */
    public function getRate()
    {
        return $this->hasOne(Rate::className(), ['id' => 'rate_id']);
    }

    /**
     * @return integer
     */
    public function getCountUserRequests()
    {
        return $this->hasMany(UserRequest::className(), ['id_user' => 'id'])->where(['status' => 1])->count();
    }

    /**
     * @return float
     */
    public function getPartnerBalance()
    {
        return $this->hasMany(History::className(), ['id_user' => 'id'])->where(['orientation' => 0])->sum('credit') * 0.07;
    }

    /* Геттер для полного имени человека */
    public function fullName()
    {
        return implode(" ", [$this->last_name, $this->first_name,  $this->patronymic]);
    }

    public static function rateList()
    {
        $rates = Rate::find()->select(['id', 'summ'])->all();

        return ArrayHelper::map($rates, 'id', 'summ');
    }

}
