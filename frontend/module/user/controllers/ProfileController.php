<?php
namespace frontend\module\user\controllers;

use Yii;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\models\PasswordChangeForm;
use frontend\models\EmailConfirm;
use frontend\components\Helper;

/**
 * ProfileController implements the CRUD actions for User model.
 */
class ProfileController extends Controller
{
    public $layout = 'vpluse';

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('view', [
            'model' => $this->findModel(Yii::$app->user->id),
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->user->id);
        $model->scenario = User::SCENARIO_PROFILE;

        if ($model->load(Yii::$app->request->post())) {
            $model->birthdate = date("Y-m-d", strtotime($model->birthdate));

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Профиль успешно обновлён.');

                return $this->redirect(['index', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);

    }

    public function actionPasswordChange()
    {
        $user = $this->findModel(Yii::$app->user->id);
        $model = new PasswordChangeForm($user);

        if ($model->load(Yii::$app->request->post()) && $model->changePassword()) {
            Yii::$app->session->setFlash('success', 'Пароль успешно изменён.');

            return $this->redirect(['index']);
        } else {
            return $this->render('passwordChange', [
                'model' => $model,
            ]);
        }
    }

    public function actionEmailChange()
    {
        $user = $this->findModel(Yii::$app->user->id);
        $model = new EmailConfirm();

        if ($model->load(Yii::$app->request->post())) {
            $model->token =  Yii::$app->security->generateRandomString();
            $model->user_id =  $user->id;
            if ($model->save()) {

                $confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['site/update-email-confirm', 'token' => $model->token]);
                $full_name = $user->fullName();
                $set_to = [$user->email => $full_name];
                $params = [
                    'confirmLink' => $confirmLink,
                    'userName' => $user->fullName()
                ];
                Helper::sendMailToUser(2, $params, $set_to);

                Yii::$app->session->setFlash('success', 'На указанный email отправлен запрос на подтверждение.');

                return $this->redirect(['index']);
            }
        }

        return $this->render('emailChange', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
