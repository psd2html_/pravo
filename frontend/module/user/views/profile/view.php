<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Мой профиль';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content__section">
    <div class="info">
        <span class="title-block"><?= Html::encode($this->title) ?></span>
        <div class="info__block">
            <p>
                <?= Html::a('Редактировать профиль', ['update'], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Изменить пароль', ['password-change'], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Изменить email', ['email-change'], ['class' => 'btn btn-primary']) ?>
            </p>
            <br>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'email:email',
                    'last_name',
                    'first_name',
                    'patronymic',
                    'birthdate',
                    'birthplace',
                    'passport_series',
                    'passport_number',
                    'passport_issued',
                    'passport_issued_date',
                    'department_code',
                    'registered_address',
                    'residential_address',
                ],
            ]) ?>
        </div>
    </div>
</div>

