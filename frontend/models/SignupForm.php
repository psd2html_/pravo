<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use frontend\components\Helper;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $re_password;
    public $phone;
    public $first_name;
    public $last_name;
    public $patronymic;
    public $city_id;
    public $confirm;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Этот адрес электронной почты уже занят.'],
            ['email', 'unique', 'targetClass' => '\frontend\models\EmailConfirm', 'message' => 'Этот адрес электронной почты уже занят.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['re_password', 'required'],
            ['re_password', 'string', 'min' => 6],
            ['re_password', 'compare', 'compareAttribute' => 'password'],

            [['phone', 'first_name', 'last_name', 'patronymic'], 'string', 'min' => 2, 'max' => 255],

            ['confirm', 'boolean'],
            ['confirm', 'isConfirm'],

            ['city_id', 'number'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'password' => 'Пароль',
            're_password' => 'Повторите пароль',
            'phone' => 'Телефон',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'patronymic' => 'Отчество',
            'city_id' => 'Город',
            'confirm' => '-Принимаю правила использования сайта',
        ];
    }

    public function isConfirm($attribute)
    {
        if (!$this->$attribute) {
            $this->addError($attribute, 'Следует согласится с правилами сайта');
        }
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {

            return null;
        }
        $user = new User();
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->status = User::STATUS_WAIT;
        $user->phone = $this->phone;
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->patronymic = $this->patronymic;
        $user->city_id = $this->city_id;
        $user->generateAuthKey();
        $user->generateEmailConfirmToken();
        $user->generateRefCode();
        $user->setReferral();

        if ($user->save()) {

            $confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['site/email-confirm', 'token' => $user->email_confirm_token]);
            $full_name = $user->fullName();
            $set_to = [$user->email => $full_name];
            $params = [
                'confirmLink' => $confirmLink,
                'userName' => $user->fullName()
            ];
            Helper::sendMailToUser(1, $params, $set_to);

            return $user;
        }

        return null;
    }
}
