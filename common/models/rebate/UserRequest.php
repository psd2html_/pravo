<?php

namespace common\models\rebate;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use common\models\User;

/**
 * This is the model class for table "{{%user_request}}".
 *
 * @property string $id
 * @property integer $id_user
 * @property integer $id_company
 * @property integer $id_section
 * @property string $account
 * @property string $status
 * @property integer $created_at
 *
 * @property User $idUser
 * @property Company $idCompany
 * @property Section $idSection
 */
class UserRequest extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => strtotime(date('Y-m-d')),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_company', 'id_section', 'account'], 'required'],
            [['id_user', 'id_company', 'id_section', 'created_at'], 'integer'],
            [['status'], 'string'],
            [['account'], 'string', 'max' => 255],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
            [['id_company'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['id_company' => 'id']],
            [['id_section'], 'exist', 'skipOnError' => true, 'targetClass' => Section::className(), 'targetAttribute' => ['id_section' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_company' => 'Id Company',
            'id_section' => 'Раздел',
            'account' => 'Счет',
            'status' => 'Состояние',
            'created_at' => 'Дата записи',
            'fullName' => 'Имя Фамилия',
            'companyName' => 'Компания',
            'statusName' => 'Текущий статус',
            'login' => 'Логин',
            //
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /* Геттер для полного имени человека */
    public function getFullName()
    {

        return $this->idUser->getFullName();
    }

    /* Геттер для логина */
    public function getLogin()
    {

        return $this->idUser->username;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'id_company']);
    }

    /* Геттер для названия компании */
    public function getCompanyName() {
        return $this->idCompany->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'id_section']);
    }

    /**
     * @return string
     */
    public function getSectionTitle()
    {
        return $this->idSection->title;
    }

    /**
     * @return string
     */
    public function getStatusName()
    {
        $status = $this->getStatusArrayNames();

        return $status[$this->status];
    }

    /**
     * @return array
     */
    public function getStatusArrayNames()
    {
        $status = ['Ожидание', 'Подтвержден', 'Отклонен'];

        return $status;
    }

}
