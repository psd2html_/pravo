<?php
/**
 * Default view of maintenance mode component for Yii framework 2.x.x version.
 * @version 0.2.1
 * @author BrusSENS (Brusenskiy Dmitry) <brussens@nativeweb.ru>
 * @link https://github.com/brussens/yii2-maintenance-mode
 * @var $title string
 * @var $message string
 */
use yii\helpers\Html;
?>
<style>
    .align_center {
        text-align: center;
    }
</style>
<div class="align_center">
    <img src="/img/logo.png" alt="logo">
</div>
<br><br>
<div class="align_center">
    <img class="logo__img" src="/img/underConstruction.gif" alt="underConstruction" width="400">
</div>
<br><br>
<div class="align_center">
    <h3><?= Html::encode($message) ?></h3>
</div>