<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use common\models\rebate\Section;

/* @var $this yii\web\View */
/* @var $searchModel common\models\rebate\SearchSection */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Разделы';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3 class="box-title"><?= $this->title ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::button('Создать раздел', ['value' => Url::to('/rebate/section/create'), 'class' => 'btn btn-success modal-button']) ?>
        </p>
        <?= GridView::widget([
            'pager' => [
                'firstPageLabel' => 'Первая страница',
                'lastPageLabel'  => 'Последняя страница'
            ],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'summary' => 'Показано {count} из {totalCount}',
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'title',
                [
                    'attribute'=>'id_base',
                    'format'=>'text',
                    'content'=>function($data){
                        return $data->getBaseSectionName();
                    },
                    'filter'=> Section::getBaseSectionsList()
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function($url){
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', 'javascript:void(0)', [
                                'value' => $url,
                                'class' => 'modal-button']);
                        },
                        'delete' => function($url){
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'class' => '',
                                'data' => [
                                    'confirm' => 'Вы действительно хотите удалить раздел?',
                                    'method' => 'post',
                                ],
                            ]);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
