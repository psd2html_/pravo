<?php
use dmstr\widgets\Alert;
use yii\bootstrap\Modal;
?>
<div class="content-wrapper">
    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
</footer>
<?php
Modal::begin([
    'id' => 'zzz_modal',
]);
echo '<div id="modal_content"></div>';
Modal::end();
?>