<?php
namespace frontend\components;

use Yii;
use yii\base\Component;
use backend\models\Configure;
use backend\models\EmailTemplates;
use common\models\User;

class Helper extends Component
{

    /**
     * @param integer $temp_id id of template
     * @param array $params
     * @return bool
     */
    public static function sendMailToUser($temp_id, $params = [], $set_to = [])
    {
        $template = EmailTemplates::findOne($temp_id);
        if(is_null($template)) {
            return false;
        }
        if (!Yii::$app->user->isGuest) {
            $current_user = User::findOne(['id' => Yii::$app->user->id]);
            $full_name = $current_user->fullName();
            if(empty($set_to)) {
                $set_to = [$current_user->email => $full_name];
            }


            $template->from = str_replace('#userName#', $full_name, $template->from);
            $template->subject = str_replace('#userName#', $full_name, $template->subject);
            $template->body_html = str_replace('#userName#', $full_name, $template->body_html);
            $template->body_text = str_replace('#userName#', $full_name, $template->body_text);
        }

        if ($params) {
            foreach ($params as $key => $param) {
                $template->from = str_replace('#'.$key.'#', $param, $template->from);
                $template->subject = str_replace('#'.$key.'#', $param, $template->subject);
                $template->body_html = str_replace('#'.$key.'#', $param, $template->body_html);
                $template->body_text = str_replace('#'.$key.'#', $param, $template->body_text);
            }
        }
        $message = Yii::$app->mailer->compose();
        $message->setFrom($template->from)
            ->setTo($set_to)
            ->setSubject($template->subject)
            ->setHtmlBody($template->body_html)
            ->setTextBody($template->body_text);

        return $message->send();

    }

    /**
     * @param integer $temp_id
     * @param array $params
     * @return bool
     */
    public static function sendMailToAdmin($temp_id, $params = [])
    {

        $admin_email = Configure::find()->where(['key' => 'admin_email'])->one();

        if(!isset($admin_email->value)) {
            return false;
        }
        $template = EmailTemplates::findOne($temp_id);
        if(is_null($template)) {
            return false;
        }
        $message = Yii::$app->mailer->compose();
        if ($params) {
            foreach ($params as $key => $param) {
                $template->from = str_replace('#'.$key.'#', $param, $template->from);
                $template->subject = str_replace('#'.$key.'#', $param, $template->subject);
                $template->body_html = str_replace('#'.$key.'#', $param, $template->body_html);
                $template->body_text = str_replace('#'.$key.'#', $param, $template->body_text);
            }

        }
        $template->from = str_replace('#userName#', Yii::$app->user->identity->fullName, $template->from);
        $template->subject = str_replace('#userName#', Yii::$app->user->identity->fullName, $template->subject);
        $template->body_html = str_replace('#userName#', Yii::$app->user->identity->fullName, $template->body_html);
        $template->body_text = str_replace('#userName#', Yii::$app->user->identity->fullName, $template->body_text);
        $message->setFrom($template->from)
            ->setTo($admin_email->value)
            ->setSubject($template->subject)
            ->setHtmlBody($template->body_html)
            ->setTextBody($template->body_text);

        return $message->send();

    }

    /**
     * @param array $id_users
     * @param integer $temp_id
     * @param array $params
     * @return bool
     */
    public static function sendMails($id_users, $temp_id, $params = [])
    {
        $users = self::find()->where(['IN', 'id', $id_users])->all();
        $template = EmailTemplates::findOne($temp_id);
        if ($params) {
            foreach ($params as $key => $param) {
                $template->body_html = str_replace('#'.$key.'#', $param, $template->body_html);
                $template->body_text = str_replace('#'.$key.'#', $param, $template->body_text);
                $template->from = str_replace('#'.$key.'#', $param, $template->from);
                $template->subject = str_replace('#'.$key.'#', $param, $template->subject);
            }
        }
        $messages = [];
        foreach ($users as $user) {
            $template->from = str_replace('#userName#', $user->fullName, $template->from);
            $template->subject = str_replace('#userName#', $user->fullName, $template->subject);
            $template->body_html = str_replace('#userName#', $user->fullName, $template->body_html);
            $template->body_text = str_replace('#userName#', $user->fullName, $template->body_text);
            $messages[] = Yii::$app->mailer->compose()
                ->setTo([$user->email => $user->fullName])
                ->setSubject($template->subject)
                ->setHtmlBody($template->body_html)
                ->setTextBody($template->body_text);
        }

        return Yii::$app->mailer->sendMultiple($messages);

    }


    /**
     * @param integer $length
     * @param string $char_types (all, lower, upper, numbers, special)
     * @return string
     */
    public static function random_string($length = 8, $char_types = "lower,numbers")
    {
        $chartypes_array=explode(",", $char_types);
        // задаем строки символов.
        //Здесь вы можете редактировать наборы символов при необходимости
        $lower = 'abcdefghijklmnopqrstuvwxyz'; // lowercase
        $upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; // uppercase
        $numbers = '1234567890'; // numbers
        $special = '^@*+-+%()!?'; //special characters
        $chars = "";
        // определяем на основе полученных параметров,
        //из чего будет сгенерирована наша строка.
        if (in_array('all', $chartypes_array)) {
            $chars = $lower . $upper. $numbers . $special;
        } else {
            if(in_array('lower', $chartypes_array))
                $chars = $lower;
            if(in_array('upper', $chartypes_array))
                $chars .= $upper;
            if(in_array('numbers', $chartypes_array))
                $chars .= $numbers;
            if(in_array('special', $chartypes_array))
                $chars .= $special;
        }
        // длина строки с символами
        $chars_length = strlen($chars) - 1;
        // создаем нашу строку,
        //извлекаем из строки $chars символ со случайным
        //номером от 0 до длины самой строки
        $string = $chars{rand(0, $chars_length)};
        // генерируем нашу строку
        for ($i = 1; $i < $length; $i = strlen($string)) {
            // выбираем случайный элемент из строки с допустимыми символами
            $random = $chars{rand(0, $chars_length)};
            // убеждаемся в том, что два символа не будут идти подряд
            if ($random != $string{$i - 1}) $string .= $random;
        }
        // возвращаем результат
        return $string;
    }
}