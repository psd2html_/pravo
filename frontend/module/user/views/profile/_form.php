<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

if(!$model->birthdate) {
    $model->birthdate = '2017-01-01';
}
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'last_name')->textInput() ?>

    <?= $form->field($model, 'first_name')->textInput() ?>

    <?= $form->field($model, 'patronymic')->textInput() ?>

    <?= $form->field($model, 'phone')->textInput() ?>

    <?= $form->field($model, 'city_id')->textInput() ?>

    <?= $form->field($model,'birthdate')->widget(DatePicker::className()) ?>

    <?= $form->field($model, 'birthplace')->textInput() ?>

    <?= $form->field($model, 'passport_series')->textInput() ?>

    <?= $form->field($model, 'passport_number')->textInput() ?>

    <?= $form->field($model, 'passport_issued')->textArea(['rows' => 6]) ?>

    <?= $form->field($model, 'passport_issued_date')->widget(DatePicker::className()) ?>

    <?= $form->field($model, 'department_code')->textInput() ?>

    <?= $form->field($model, 'registered_address')->textArea(['rows' => 6]) ?>

    <?= $form->field($model, 'residential_address')->textArea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
