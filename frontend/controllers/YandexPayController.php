<?php

namespace frontend\controllers;

class YandexPayController extends \yii\web\Controller
{
    public function actionEvent()
    {
        return $this->render('event');
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

}
