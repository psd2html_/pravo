<?php

use yii\helpers\Html;
use backend\widgets\CustomGridWidget;
use yii\helpers\Url;
use common\models\rebate\Company;

/* @var $this yii\web\View */
/* @var $searchModel common\models\rebate\SearchCompany */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Компании';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3 class="box-title"><?= $this->title ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::button('Создать компанию', ['value' => Url::to('/rebate/company/create'), 'class' => 'btn btn-success modal-button']) ?>
        </p>
        <?= CustomGridWidget::widget([
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'name',
                //'sectionTitle',
                [
                    'attribute'=>'id_section',
                    'format'=>'text',
                    'content'=>function($data){
                        return $data->getSectionTitle();
                    },
                    'filter'=> Company::getSectionsList()
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function($url){
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', 'javascript:void(0)', [
                                'value' => $url,
                                'class' => 'modal-button']);
                        },
                        'delete' => function($url){
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'class' => '',
                                'data' => [
                                    'confirm' => 'Вы действительно хотите удалить компанию?',
                                    'method' => 'post',
                                ],
                            ]);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
