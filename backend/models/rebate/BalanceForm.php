<?php
namespace backend\models\rebate;

use yii\base\Model;
use common\models\rebate\Company;
use yii\helpers\ArrayHelper;

/**
 * Password reset form
 */
class BalanceForm extends Model
{
    public $balance;
    public $company;
    public $note;

    public function rules()
    {
        return [
            [['balance', 'company', 'note'], 'required'],
            [['note', 'company'], 'string'],
            [['balance'], 'number', 'min' => 0],
        ];
    }

    public function attributeLabels()
    {
        return [
            'balance' => 'Сумма($)',
            'company' => 'Компания',
            'note' => 'Примечание',
        ];
    }

    public function companyNamesList() {

        $companies = Company::find()
            ->select('name')
            ->orderBy('name')
            ->all();

        return ArrayHelper::map($companies, 'name', 'name');

    }
}