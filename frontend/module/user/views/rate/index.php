<?php
/* @var $this yii\web\View */
/* @var $rates array */
/* @var $current_rate int */

$this->title = 'Тарифы';
?>

<div class="content__section">
    <div class="all-bid">
        <span class="title-block"><?= $this->title ?></span>
<?php
$current_sum = 0;
if($current_rate_info) {
    $current_sum = $current_rate_info->summ;
?>
    <p>Ваш текущий тариф: <strong><?= $current_rate_info->summ ?> </strong></p>
    <p><?= $current_rate_info->description ?></p>
    <br>
<?php
} else {
?>
    <p>У вас пока нету тарифа.</p>
    <br>
<?php
}
?>
        <div class="all-bid__table">
            <table class="rtable rtable--flip">
                <thead>
                <tr>
                    <th>Тариф</th>
                    <th>Описание</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($rates) {
                    foreach ($rates as $rate) {
                    ?>
                        <tr>
                            <td><?= $rate->summ ?> рублей</td>
                            <td><?= $rate->description ?></td>
                            <td>
                                <?php if($rate->summ < $current_sum) { ?>
                                    тариф недоступен
                                <?php } elseif($rate->id == $current_rate) { ?>
                                    <button type="button" class="btn-success btn-sm">
                                        <span class="glyphicon glyphicon-ok"></span>
                                    </button>
                                <?php } else { ?>
                                <a href="/user/rate/order/?id=<?= $rate->id ?>">
                                    <button type="button" class="btn btn-default btn-lg">
                                        <span class="glyphicon glyphicon-star"></span>
                                        Заказать
                                    </button>
                                </a>
                                <?php }?>
                            </td>
                        </tr>
                    <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="4">Тарифов нет</td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>