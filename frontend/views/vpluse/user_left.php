<?php
use yii\helpers\Url;

$current_action = Yii::$app->controller->action->id;
$current_controller = Yii::$app->controller->id;
$active_balance = '';
if($current_controller == 'balance' || $current_controller == 'pay') {
    $active_balance = ' active';
}
?>
<div class="sidebar">
    <ul class="sidebar__nav">
        <li class="sidebar__row">
            <a class="sidebar__item sidebar__item-support<?php echo $current_controller == 'rate' ? ' active-empty' : ''?>" href="<?= Url::to(['/user/rate']) ?>">
                <span class="sidebar__title">Выбор тарифа</span>
            </a>
        </li>
        <li class="sidebar__row">
            <a class="sidebar__item sidebar__item-profile<?php echo $current_controller == 'profile' ? ' active-empty' : ''?>" href="<?= Url::to(['/user/profile']) ?>">
                <span class="sidebar__title">Мой профиль</span>
            </a>
        </li>
        <li class="sidebar__row">
            <a class="sidebar__item sidebar__item-balance<?php echo $current_controller == 'balance' ? ' active-empty' : ''?>" href="<?= Url::to(['/balance']) ?>">
                <span class="sidebar__title">Финансы</span>
            </a>
        </li>
    </ul>
    <a class="show-menu" href="#?">
        <span class="show-menu__ico"></span>
        <span class="show-menu__title">свернуть</span>
    </a>
</div>