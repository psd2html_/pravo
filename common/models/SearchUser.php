<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * SearchUser represents the model behind the search form about `common\models\User`.
 */
class SearchUser extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'rate_id'], 'number'],
            [['first_name', 'email', 'phone'], 'string'],
            [['birthdate'], 'date', 'format' => 'php: d.m.Y'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $query = null)
    {
        if(is_null($query)) {
            $query = User::find();
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                /*'first_name' => [
                    'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC],
                    'desc' => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'username',
                'balance',
                'email',*/
                'created_at'
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->birthdate) {
            $mysql_date_start = date("Y-m-d", strtotime($this->birthdate));
            $mysql_date_end = date("Y-m-d", strtotime($this->birthdate) + (60*60*24));
            $query->andFilterWhere(['>=', 'birthdate', $mysql_date_start]);
            $query->andFilterWhere(['<', 'birthdate', $mysql_date_end]);
        }
        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['rate_id' => $this->rate_id]);
        $query->andFilterWhere(['like', 'email', $this->email]);
        $query->andFilterWhere(['like', 'phone', $this->phone]);

        $query->andWhere('first_name LIKE "%' . $this->first_name . '%" ' .
            'OR last_name LIKE "%' . $this->first_name . '%"
             OR patronymic LIKE "%' . $this->first_name . '%"'
        );

        $query->andFilterWhere(['like', 'email', $this->email]);


        return $dataProvider;
    }
}
